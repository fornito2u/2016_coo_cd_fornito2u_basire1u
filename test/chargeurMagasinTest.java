package test;


import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;
import XML.ChargeurMagasin;


public class chargeurMagasinTest 
{
	/**
	 * M�thode de test de la m�thode chargerMagasin quand l'utilisation est fonctionnel
	 */
	@test
	public void chargerMagasinNormal() throws IOException
	{
		//Initialisation
		ChargeurMagasin mag=new ChargeurMagasin("musicbrainzSimple");
		
		//Test
		mag.chargerMagasin();
	}
	
	/**
	 * M�thode de test de la m�thode chargerMagasin quand le r�pertoire n'�xiste pas
	 */
	@test(expected = IOException.class)
	public void testChargerMagasin_Exception() throws FileNotFoundException
	{
		//Initialisation
		ChargeurMagasin mag = new ChargeurMagasin("Faut");
		
		//Test
		mag.chargerMagasin();
	}

}