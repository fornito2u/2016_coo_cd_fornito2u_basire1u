package test;

public class TestTri {
	
	@test
	public void testTriAlbum()
	{
		//Initialisation
		ChargeurMagasin mag=new ChargeurMagasin("musicbrainzSimple");
		mag.ajouteCd(new CD("Marvin", "Le d�ni"));
		mag.ajouteCd(new CD("Flo", "Le bitbucket infernal"));
		mag.ajouteCd(new CD("Felix", "Une musique de chat"));
		
		//Test
		mag.trier(new ComparateurAlbum());
		
		assertEquals("le premier cd n'a pas �t� trier","Le bitbucket infernal",mag.getCd(0).getNomCD());
	}
	
	@test
	public void testTriArtiste()
	{
		//Initialisation
		ChargeurMagasin mag=new ChargeurMagasin("musicbrainzSimple");
		mag.ajouteCd(new CD("Marvin", "Le d�ni"));
		mag.ajouteCd(new CD("Flo", "Le bitbucket infernal"));
		mag.ajouteCd(new CD("Felix", "Une musique de chat"));
	
		//Test
		mag.trier(new ComparateurArtiste());
		
		assertEquals("le premier cd n'a pas �t� trier","Felix",mag.getCd(0).getnomArtiste());
	}
	
}
